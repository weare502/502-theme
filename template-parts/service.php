<div class="service">
<?php $tax = get_field('service_taxonomy'); ?>
	
	<?php if ( ! empty( get_field('service_intro') ) ) : ?>
		<div class="intro clear">
			<h2 class="h2 centered"><?php the_field('service_intro'); ?></h2>
			<p><?php the_field('service_intro_paragraph'); ?></p>
		</div>
	<?php endif; ?>

	<?php $top_gallery = get_field('service_top_gallery');
		
		if ( ! empty( $top_gallery ) ) : ?>
			
			<div class="gallery gallery-columns-2 clear">
				
				<?php foreach ( $top_gallery as $item ) : ?>
					
					<img src="<?php echo $item['url']; ?>" class="gallery-item" />

				<?php endforeach; ?>

			</div>

	<?php endif; ?>

	<h2 class="h2 centered title"><?php the_field('service_case_study_title'); ?>Branding Stuff</h2>

	<div class="case-studies wrap">
		<div class="case-study accordion">
		
		<?php $case_studies = new WP_Query( array( 
				'post_type' => 'case-study', 
				'posts_per_page' => -1,
				'tax_query' => array(
					array(
						'taxonomy' => 'filter',
						'field'    => 'slug',
						'terms'    => $tax->slug,
					)
				),
			) ); 

			foreach ( $case_studies->posts as $cs ) :
				$banner = get_field( 'case_study_banner', $cs->ID );
				$link_image = get_field( 'case_study_linked_image', $cs->ID );
			?>
				
					<img src="<?php echo $banner['sizes']['case_study_banner']; ?>" />

					<div class="content">
						<div class="wrapper">
							<h3><?php echo get_the_title( $cs->ID ); ?></h3>
							<p><?php the_field( 'case_study_short_description', $cs->ID ); ?><br><a href="<?php echo get_permalink($cs->ID); ?>">Read More + </a></p>
						</div>
						<a href="<?php echo get_permalink($cs->ID); ?>"><img src="<?php echo $link_image['url']; ?>" /></a>
					</div>
			
			<?php endforeach; ?>

		</div>
	</div>

	<h2 class="h2 centered title">Other Related Projects</h2>

	<?php $portfolio_items = new WP_Query( array(
			'post_type' => 'portfolio',
			'posts_per_page' => 6,
			'orderby' => 'rand',
			'tax_query' => array(
				array(
					'taxonomy' => 'filter',
					'field'    => 'slug',
					'terms'    => $tax->slug,
				)
			),
		) );
	?>

	<div class="gallery gallery-columns-3 wrap grid">
	<div class="gutter-sizer"></div>

		<?php foreach ( $portfolio_items->posts as $item ): ?>
			<?php $terms = get_the_terms( $item->ID, 'filter');
				$term_css = array();
				foreach ( $terms as $term ){
					$term_css[] = $term->slug;
				}
			?>

			<a href="<?php echo get_the_permalink( $item->ID ); ?>" class="gallery-item <?php echo implode( ' ', $term_css );?>">
				<img src="<?php echo wp_get_attachment_image_src( get_post_thumbnail_id( $item->ID ), 'medium' )[0]; ?>" />
			</a>

		<?php endforeach; ?>

	</div>

	<h3 class="centered"><a href="/portfolio" class="button">See our full portfolio</a></h3>

</div>