<?php the_content(); ?>

<div class="portfolio">

	<div class="filter-buttons clear">
			<!-- <p class="h2 centered">Filter</p> -->
			<button data-filter="gallery-item">All Portfolio Items</button>
			<?php $terms = get_terms('filter');

			foreach ( $terms as $term ) : ?>

				<?php if ( $term->slug === 'featured' || $term->slug === 'award-winning' ) continue;  ?>
				
				<button data-filter="<?php echo $term->slug; ?>"><?php echo $term->name; ?></button>

			<?php endforeach; ?>
	</div>

	<?php $portfolio_items = new WP_Query( array(
			'post_type' => 'portfolio',
			'posts_per_page' => -1,
		) );
	?>

	<div class="gallery gallery-columns-3 wrap grid">
	<div class="gutter-sizer"></div>

		<?php foreach ( $portfolio_items->posts as $item ): ?>
			<?php $terms = get_the_terms( $item->ID, 'filter');
				$term_css = array();
				foreach ( $terms as $term ){
					$term_css[] = $term->slug;
				}
			?>

			<a href="<?php echo get_the_permalink( $item->ID ); ?>" class="gallery-item <?php echo implode( ' ', $term_css );?>">
				<img src="<?php echo wp_get_attachment_image_src( get_post_thumbnail_id( $item->ID ), 'medium' )[0]; ?>" />
			</a>

		<?php endforeach; ?>

	</div>
	
	<h2 class="h2 centered">Read through our case studies</h2>

	<div class="case-studies wrap">
		<div class="case-study accordion">
		<?php $case_studies = new WP_Query( array( 'post_type' => 'case-study', 'posts_per_page' => -1 ) ); 

			foreach ( $case_studies->posts as $cs ) :
				$banner = get_field( 'case_study_banner', $cs->ID );
				$link_image = get_field( 'case_study_linked_image', $cs->ID );
			?>
				
					<img src="<?php echo $banner['sizes']['case_study_banner']; ?>" />
					<div class="content">
						<div class="wrapper">
							<h3><?php echo get_the_title( $cs->ID ); ?></h3>
							<p><?php the_field( 'case_study_short_description', $cs->ID ); ?><br><a href="<?php echo get_permalink($cs->ID); ?>">Read More + </a></p>
						</div>
						<a href="<?php echo get_permalink($cs->ID); ?>"><img src="<?php echo $link_image['url']; ?>" /></a>
					</div>
				
			
			<?php endforeach; ?>
		</div>
	</div>

</div>