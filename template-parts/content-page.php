<?php
/**
 * Template part for displaying page content in page.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package 502 Media Group
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<style type="text/css">
		.entry-header {
			background-image: url('<?php $header_bg = get_field("page_header_image"); echo empty( $header_bg ) ? "" : $header_bg["url"]; ?>');
			background-position: <?php the_field('page_header_image_position'); ?> center;
		}
	</style>
	<header class="entry-header">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php if ( is_page( 1536 ) ) {
			get_template_part( 'template-parts/team' );
		} elseif ( is_page( 1760 ) ) {
			get_template_part( 'template-parts/portfolio' );
		} elseif ( is_page( 1812 ) ) {
			get_template_part( 'template-parts/services' );
		} elseif ( is_page( 64 ) ) {
			get_template_part( 'template-parts/contact' );
		} elseif ( $post->post_parent === 1812 ) {
			get_template_part( 'template-parts/service' );
		} else {
			the_content();
		} ?>

	</div><!-- .entry-content -->

	<footer class="entry-footer clear">
		<?php if ( ! empty( get_field( 'footer_cta' ) ) ) : ?>
			<?php the_field('footer_cta'); ?>
		<?php else : ?>
			<p>Ready to get started? <a href="/contact">Let's Chat</a></p>
		<?php endif; ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->

