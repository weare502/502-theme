	<div id="services">

	<div class="intro clear">
		<h2 class="h2 centered"><?php the_field('services_intro_title'); ?></h2>
		<p><?php the_field('services_intro_paragraph'); ?></p>
	</div>

	<?php $top_gallery = get_field('services_top_gallery');
		
		if ( ! empty( $top_gallery ) ) : ?>
			
			<div class="gallery gallery-columns-2 clear">
				
				<?php foreach ( $top_gallery as $item ) : ?>
					
					<img src="<?php echo $item['url']; ?>" class="gallery-item" />

				<?php endforeach; ?>

			</div>

	<?php endif; ?>

	<?php if ( have_rows('services_list') ) : ?>

	<section class="services-container">

		<h2 class="centered services-title">Conversation Topics</h2>

		<div class="services-list">

			<?php while ( have_rows('services_list' ) ) : the_row(); ?>

				<div class="service">
					<img src="<?php echo get_template_directory_uri() . '/images/' . get_sub_field('title') . '.svg'; ?>" />
					<h3 class="title"><?php the_sub_field('title'); ?></h3>
					<p class="description"><?php the_sub_field('description'); ?></p>
				</div>

			<?php endwhile; ?>

		</div>

	</section>

	<?php endif; ?>

	<?php $bottom_gallery = get_field('services_bottom_gallery');
		
		if ( ! empty( $bottom_gallery ) ) : ?>
			
			<div class="gallery gallery-columns-2 clear">
				
				<?php foreach ( $bottom_gallery as $item ) : ?>
					
					<img src="<?php echo $item['url']; ?>" class="gallery-item" />

				<?php endforeach; ?>

			</div>

	<?php endif; ?>

</div>