<div class="meet-502">

<?php if ( ! empty( get_field('meet_page_intro') ) ) : ?>
	<div class="intro clear">
	<h2 class="h2 centered"><?php the_field('meet_page_intro'); ?></h2>
	<p><?php the_field('meet_intro_paragraph'); ?></p>
	</div>
<?php endif; ?>

<?php $high_gallery = get_field('meet_high_image_gallery');
	
	if ( ! empty( $high_gallery ) ) : ?>
		
		<div class="gallery gallery-columns-4 clear">
			
			<?php foreach ( $high_gallery as $item ) : ?>
				
				<img src="<?php echo $item['url']; ?>" class="gallery-item skrollable" data-bottom-top="position: relative; -webkit-filter: grayscale(100%); " data--50-top="-webkit-filter: grayscale(0%);" />

			<?php endforeach; ?>

		</div>

<?php endif; ?>

<style>
	.office-description {
		background-image: url('<?php $map = get_field("meet_office_description_bg_image"); echo empty($map) ? "" : $map["url"]; ?>');
	}
</style>

<div class="office-description clear">
	
	<div class="wrap">
		<h2 class="h2 centered"><?php the_field('meet_office_title'); ?></h2>
		<p><?php the_field('meet_office_description') ?></p>
	</div>
</div>

<?php $mid_gallery = get_field('meet_mid_image_gallery');
	
	if ( ! empty( $mid_gallery ) ) : ?>
		
		<div class="gallery gallery-columns-2 clear">
			
			<?php foreach ( $mid_gallery as $item ) : ?>
				
				<img src="<?php echo $item['url']; ?>" class="gallery-item skrollable" data-bottom-top="position: relative; -webkit-filter: grayscale(100%); " data-center-top="-webkit-filter: grayscale(0%);" />

			<?php endforeach; ?>

		</div>

<?php endif; ?>

<?php if ( have_rows('team_members') ) : ?>
	
	<div id="team-members">
		<h2 class="h2 centered">The Team</h2>
		
		<?php while( have_rows('team_members') ) : the_row(); ?>
			<?php $image = get_sub_field('image'); ?>
			<div class="team-member">
				<img src="<?php echo $image['url']; ?>" class="skrollable" data-bottom-top="position: relative; -webkit-filter: grayscale(100%); " data-center-top="-webkit-filter: grayscale(0%);">
				<h3 class="name"><?php the_sub_field('name'); ?></h3>
				<p class="title"><?php the_sub_field('title'); ?></p>
				<p class="bio"><?php the_sub_field('bio'); ?></p>
			</div>

		<?php endwhile; ?>

	</div>
		
<?php endif; ?>

<?php $low_gallery = get_field('meet_low_image_gallery');
	
	if ( ! empty( $low_gallery ) ) : ?>
		
		<div class="gallery gallery-columns-4 clear">
			
			<?php foreach ( $low_gallery as $item ) : ?>
				
				<img src="<?php echo $item['url']; ?>" class="gallery-item skrollable" data-bottom-top="position: relative; -webkit-filter: grayscale(100%); " data-center-top="-webkit-filter: grayscale(0%);" />

			<?php endforeach; ?>

		</div>

<?php endif; ?>

</div>