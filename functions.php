<?php
/**
 * 502 Media Group functions and definitions.
 *
 * @link https://codex.wordpress.org/Functions_File_Explained
 *
 * @package 502 Media Group
 */

if ( ! function_exists( 'fmg_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function fmg_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on 502 Media Group, use a find and replace
	 * to change 'fmg' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'fmg', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary Menu', 'fmg' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See https://developer.wordpress.org/themes/functionality/post-formats/
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'fmg_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );

	add_image_size( 'portfolio', 400, 200, array('center', 'center') );

	add_image_size( 'case_study_banner', 1200, 200, array('center', 'center') );
}
endif; // fmg_setup
add_action( 'after_setup_theme', 'fmg_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function fmg_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'fmg_content_width', 1200 );
}
add_action( 'after_setup_theme', 'fmg_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function fmg_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'fmg' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'fmg_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function fmg_scripts() {
	wp_enqueue_style( 'fmg-style', get_stylesheet_uri(), array(), '20150103' );

	wp_enqueue_script( 'fmg-imagesloaded', get_template_directory_uri() . '/bower_components/imagesloaded/imagesloaded.pkgd.min.js', array(), '20120216', true );

	wp_enqueue_script( 'fmg-isotope', get_template_directory_uri() . '/js/isotope.pkgd.min.js', array( 'fmg-imagesloaded' ), '20120216', true );

	wp_enqueue_script( 'fmg-fitvids', get_template_directory_uri() . '/bower_components/fitvids/jquery.fitvids.js', array( 'jquery' ), '20120216', true );

	wp_enqueue_script( 'fmg-skrollr', get_template_directory_uri() . '/bower_components/skrollr/dist/skrollr.min.js', array( 'jquery' ), '20120206', true );

	wp_enqueue_script( 'fmg-navigation', get_template_directory_uri() . '/js/min/navigation-min.js', array( 'jquery', 'fmg-isotope', 'jquery-ui-accordion', 'jquery-masonry', 'fmg-fitvids'), '20120207', true );

	wp_enqueue_script( 'fmg-videojs', get_template_directory_uri() . '/bower_components/video.js/dist/video-js/video.js', array( 'jquery' ), '20120206', true );

	wp_enqueue_script( 'fmg-imagesloaded', get_template_directory_uri() . '/bower_components/imagesloaded/imagesloaded.js', array( 'jquery' ), '20120206', true );

	wp_enqueue_script( 'fmg-bigvideo', get_template_directory_uri() . '/bower_components/bigvideo.js/lib/bigvideo.js', array( 'jquery' ), '20120206', true );

	wp_enqueue_script( 'fmg-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'fmg_scripts' );

add_filter('the_content', function( $content ){
	return "<div class='the-content'>" . $content . "</div>";
}, 9999 );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

// custom post type
function fmg_post_types() 
{	
	// portfolio
	register_post_type( 'Portfolio',
		array(
			'labels' 		=> array(
			'name' 			=> __( 'Portfolio', 'fmg' ),
			'singular_name' => __( 'Portfolio', 'fmg' ),
			'add_new' 		=> __( 'Add New Project', 'fmg' ),
			'add_new_item' 	=> __( 'Add New Project', 'fmg' ),
			'edit' 			=> __( 'Edit', 'fmg' ),
			'edit_item' 	=> __( 'Edit Project', 'fmg' ),
			'view' 			=> __( 'View', 'fmg' ),
			'view_item' 	=> __( 'View Project', 'fmg' ),
		),
			'public' 		=> true,
			'supports' 		=> array( 'title', 'editor', 'thumbnail' ),
			'menu_icon'			=> 'dashicons-images-alt'
		)
	);

	register_post_type( 'case-study',
		array(
			'labels' 		=> array(
			'name' 			=> __( 'Case Studies', 'fmg' ),
			'singular_name' => __( 'Case Study', 'fmg' ),
			'add_new' 		=> __( 'Add New Case Study', 'fmg' ),
			'add_new_item' 	=> __( 'Add New Case Study', 'fmg' ),
			'edit' 			=> __( 'Edit', 'fmg' ),
			'edit_item' 	=> __( 'Edit Case Study', 'fmg' ),
			'view' 			=> __( 'View', 'fmg' ),
			'view_item' 	=> __( 'View Case Study', 'fmg' ),
			'not_found'     => __( 'No case studies found', 'fmg' ),
		),
			'public' 		=> true,
			'supports' 		=> array( 'title', 'editor', 'thumbnail' ),
			'menu_icon'			=> 'dashicons-welcome-learn-more'
		)
	);
	
	// testimonial
	register_post_type( 'Testimonial',
		array(
			'labels' 		=> array(
			'name' 			=> __( 'Testimonials', 'fmg' ),
			'singular_name' => __( 'Testimonial', 'fmg' ),
			'add_new' 		=> __( 'Add New Testimonial', 'fmg' ),
			'add_new_item' 	=> __( 'Add New Testimonial', 'fmg' ),
			'edit' 			=> __( 'Edit', 'fmg' ),
			'edit_item' 	=> __( 'Edit Testimonial', 'fmg' ),
			'view' 			=> __( 'View', 'fmg' ),
			'view_item' 	=> __( 'View Testimonial', 'fmg' ),
		),
			'public' 		=> true,
			'supports' 		=> array( 'title', 'editor' ),
			'menu_icon'			=> 'dashicons-testimonial'
		)
	);

}
add_action( 'init', 'fmg_post_types' );

// portfolio filters
function fmg_filter_init() 
{
	// initialize taxonomy labels
    $labels = array(
        'name'              => _x( 'Filters', 'taxonomy general name', 'fmg' ),
        'singular_name'     => _x( 'Filter', 'taxonomy singular name', 'fmg' ),
        'search_items'      =>  __( 'Search Types', 'fmg' ),
        'all_items'         => __( 'All Filters', 'fmg' ),
        'parent_item'       => __( 'Parent Filter', 'fmg' ),
        'parent_item_colon' => __( 'Parent Filter:', 'fmg' ),
        'edit_item'         => __( 'Edit Filters', 'fmg' ),
        'update_item'       => __( 'Update Filter', 'fmg' ),
        'add_new_item'      => __( 'Add New Filter', 'fmg' ),
        'new_item_name'     => __( 'New Filter Name', 'fmg' ),
    );
    // taxonomy for filters
    register_taxonomy(
        'filter', 
        
        array('portfolio', 'case-study'), 
            array(
                'hierarchical' => true,
                'labels'       => $labels,
                'show_ui'      => true,
                'query_var'    => true,
                'rewrite'      => array( 
                'slug'         => 'filter' 
                ),
            )
        );
}
add_action( 'init', 'fmg_filter_init' );


