/**
 * navigation.js
 *
 * Handles toggling the navigation menu for small screens and enables tab
 * support for dropdown menus.
 */


( function( $ ) {

	$('#site-navigation').headroom({
		"offset": 100,
		  "tolerance": 10,
		  "classes": {
		    "initial": "animated",
		    "pinned": "slideDown",
		    "unpinned": "slideUp"
		  }
	});

	$('.menu-toggle').on( 'click', toggleMenu );

	function toggleMenu(){

		$('#primary-menu').toggleClass('open');
		$('#site-navigation').toggleClass('open');
		$('.main-menu-container').toggleClass('open');

	}

	var $grid = $('.grid').imagesLoaded( function() {
		// init Isotope after all images have loaded
		$grid.isotope({
			itemSelector: '.gallery-item',
			percentPosition: true,
			layoutMode: 'fitRows',
			fitRows: {
				gutter: '.gutter-sizer'
			},
			// filter: '.featured'
		});

		$grid.css('opacity', 1 );
	});

	$('.filter-buttons').on( 'click', 'button', function() {
	    var filterValue = $( this ).attr('data-filter');
	    $grid.isotope({ filter: '.' + filterValue });
	});

	$('.case-study-toggle').on('click', function(){
		$(this).siblings().slideToggle();
	});

	var accordion = $( ".accordion" ).accordion({
		heightStyle: "content",
		collapsible: true,
		header: "> img",
		active: false
	});

	$('#main').fitVids();

	$blogGrid = $('.blog #main').imagesLoaded( function(){

		function masonryInit(){
			$blogGrid.masonry({
			  // options
			  itemSelector: 'article.post',
			  columnWidth: 50,
			  isFitWidth: true
			});
		}

		var timer;

		$(window).on('resize', function() {
		    if(timer) {
		        window.clearTimeout(timer);
		    }

		    timer = window.setTimeout(function() {
		        if( $(window).width() < 520 ){

		        	if ( $('#main').hasClass('masonry') ){
		        		$blogGrid.masonry('destroy');	
		        	}
		        	
		        } else if ( $('#main').hasClass('masonry') ){
		        	return;
		        } else {
		        	masonryInit();
		        }
		    }, 200);
		});

		$(window).trigger('resize');

	});

	$('#commentform #submit').val( "Post Comment +" );
	$('#commentform #comment').attr( 'rows', 5 );

	// skrollr.init({
	// 	forceHeight: false,
	// 	mobileCheck: function() {
 //           //hack - forces mobile version to be off
 //           return false;
 //       }
	// });

	var $set = $('#gform_fields_4').children();    
	
	$set.slice(0, 3).wrapAll('<div class="centered choices"/>');    

})(jQuery)

