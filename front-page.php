<?php get_header(); ?>
	
	<div class="home-wrap">
	<h1><?php include( get_stylesheet_directory() . "/logo.svg"); ?><span class="screen-reader-text">502 Media Group</span></h1>


		<?php if ( have_rows('home_links' ) ) : ?>

			<div class="cta-links">

				<?php while ( have_rows('home_links' ) ) : the_row(); ?>
			
					<a href="<?php the_sub_field('link'); ?>" class="cta-link"><?php the_sub_field('text'); ?></a>
				
				<?php endwhile; ?>
			
			</div>

		<?php endif; ?>
	</div>

<?php get_footer(); ?>