<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package 502 Media Group
 */

?>

	</div><!-- #content -->

<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="site-info">
		    <p>Made with <i class="fa fa-heart"><span class="screen-reader-text">love</span></i> in Manhattan KS <br> 502 Media Group &copy; 2007 - <?php echo date('Y'); ?></p>
        <p class="social">
          <a href="http://facebook.com/502mediagroup/" target="_blank"><i class="fa fa-facebook"></i></a>
          <a href="http://twitter.com/502mediagroup/" target="_blank"><i class="fa fa-twitter"></i></a>
          <a href="http://instagram.com/502mediagroup/" target="_blank"><i class="fa fa-instagram"></i></a>
        <!--   <a href="http://pinterest.com/502mediagroup/" target="_blank"><i class="fa fa-pinterest"></i></a> -->
        </p>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<script src="<?php echo get_template_directory_uri(); ?>/js/headroom.js/dist/headroom.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/headroom.js/dist/jQuery.headroom.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/modernizr.js"></script>
<?php wp_footer(); ?>
<?php if ( is_front_page() ) : ?>
  <script type="text/javascript">
    (function($){
      
      $(window).load(function(){

        var BV = new jQuery.BigVideo();
        BV.init();
        if (Modernizr.touch) {
          
        } else {
          
          BV.show( '<?php echo get_template_directory_uri(); ?>/videos/home.mp4',
          {
            ambient: true,
          });
        }

      });

    })(jQuery)
  </script>

<?php endif; ?>

</body>
</html>
